export class Hero {
    id : number;
    name : string;
    color : string;
    RangerID : string;
    phone : string;
    profile : string;
}