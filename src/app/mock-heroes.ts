import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Wolverine', color : 'Wolverine', RangerID : 'green11', phone : '0909090909', profile : 'assets/image/wolverine.jpg'},
  { id: 12, name: 'Spider-Man', color : 'Spider-Man', RangerID : 'red12', phone : '0909090909', profile : 'assets/image/spiderman.jpg'},
  { id: 13, name: 'Thor', color : 'Thor', RangerID : 'blueblack13', phone : '0909090909', profile : 'assets/image/thor.jpg'},
  { id: 14, name: 'Iron Man', color : 'Iron-Man', RangerID : 'black14', phone : '0909090909', profile : 'assets/image/ironman.jpg'},
  { id: 15, name: 'Hulk', color : 'Hulk', RangerID : 'golden15', phone : '0909090909', profile : 'assets/image/hulk.jpg'},
  { id: 16, name: 'Captain America', color : 'Captain-America', RangerID : 'white', phone : '0909090909', profile : 'assets/image/captain-america.jpg'},
  { id: 17, name: 'Daredevil', color : 'Daredevil', RangerID : 'blue17', phone : '0909090909', profile : 'assets/image/daredevil.jpg'},
  { id: 18, name: 'Punisher', color : 'Punisher', RangerID : 'silver18', phone : '0909090909', profile : 'assets/image/pulisher.jpg'},
  { id: 19, name: 'Deadpool', color : 'Deadpool', RangerID : 'candy19', phone : '0909090909', profile : 'assets/image/deadpool.jpg'},
  { id: 18, name: 'Silver Surfer', color : 'Silver-Surfer', RangerID : 'silver18', phone : '0909090909', profile : 'assets/image/silver-serface.jpg'},
  { id: 19, name: 'Gambit', color : 'Gambit', RangerID : 'candy19', phone : '0909090909', profile : 'assets/image/Gambit.jpg'}

];